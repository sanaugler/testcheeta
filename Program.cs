﻿using System;
using ASC.Trust.API.SDK;
using ASC.Trust.API.SDK.Models;
using ASC.Trust.API.SDK.Models.Response;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.DependencyInjection;
using ASC.Trust.API.SDK.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TestCheeta
{
    public class SdkDemo
    {
        private HandlerFactory _cheetahHandlerFactory;
        private AccountHandler _accountHandler;

        public SdkDemo(HandlerFactory cheetahHandlerFactory)
        {
            _cheetahHandlerFactory = cheetahHandlerFactory;
            _accountHandler = cheetahHandlerFactory.CreateHandler<AccountHandler>();
        }

        static void Main(string[] args)
        {

            var host = CreateHostBuilder(args).Build();
            host.Services.GetRequiredService<SdkDemo>().Run();
        }

        public void Run()
        {
            var accounts = GetAccounts();
            Console.WriteLine(accounts[0]); // want to write out the first list element
        }

        public static IHostBuilder CreateHostBuilder(string[] args)
        {
            return Host.CreateDefaultBuilder(args).ConfigureServices(services =>
            {
                services.AddTransient<SdkDemo>();
                services.AddTransient<HandlerFactory, IpsAccountFactory>(); // services.AddTransient< Interface, Concrete>();
            });
        }

        public List<AccountApiModel> GetAccounts()
        {
            var accountResponse = await _accountHandler.Accounts();
            if (accountResponse.)
            {
                return accountResponse.Data;
            }
            // log errors here
            return new List<AccountApiModel>();
        }
    }

    class IpsAccountFactory : HandlerFactory
    {
        public static string apikey = "FD3VexCQy3ECMQWwRwhqRgQdMcUfjh8bJSHiDNDCszrxtIQh0Qp5iKHS4GNwIWzjOCIGPfCTXYOZwZAmT6lC9PXEObMmS5caEpkQLwctwpOuKwGy7nuDwTFOccfBnJzo";
        public static string bearerToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVc2VyR3VpZCI6ImEyN2JkNTc0LTM1YmEtNDE5YS1iNzU3LWE3YmFkNGY1OTdlMyIsImh0dHA6Ly9zY2hlbWFzLnhtbHNvYXAub3JnL3dzLzIwMDUvMDUvaWRlbnRpdHkvY2xhaW1zL25hbWUiOiJTcGVuY2VyTiIsIlN5c3RlbSI6IkZhbHNlIiwiU291cmNlIjoiRGV2IFBvcnRhbCBLZXkiLCJleHAiOjE2NDMyMjg1NDMsImlzcyI6Imh0dHBzOi8vd3d3LnRydXN0YXNjLmNvbS8iLCJhdWQiOiJodHRwczovL3d3dy50cnVzdGFzYy5jb20vIn0.bJqh0G1E3jOceXCpUp5r31EI7KqbNNkJLp8gK1-zF9U";
        public static string user = "SpencerN";
        public static string pass = "BallState#Student@21";
        public static string baseUrl = "https://ballstatetestingwebapi.accutech-systems.net/api/v6/";
        public static ILogger logger = LoggerFactory.Create(configure => configure.AddConsole()).CreateLogger("console");

        public IpsAccountFactory(string userName, string passWord, string baseUrl, string apiKey, ILogger logger) :
            base(userName, passWord, baseUrl, apiKey, logger)
        {

        }
    }

    class Program
    {
        private SdkDemo Demo { get; set; }


        /*  public AccountController

                public AccountController(IHandlerFactory factory)

                  
            
            public TestAccountController

                // moq 4
                public TestAcountControllerGetsAccounts(){
                    var mockHandlerFactory = new Mock<IHandlerFactor>();
                    var accountController = new AccountController(mockHandlerFactory.Object)
                }
        */
    }
}
